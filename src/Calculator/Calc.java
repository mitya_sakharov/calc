package Calculator;
import java.util.*;
public class Calc {
    public static void main(String args[]) throws java.io.IOException {
        double db1 = 0, db2 = 0;
        int choice;
        Scanner sc = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("������� ������ �����:");
                db1 = (double) sc.nextDouble();
            } catch (InputMismatchException exc) {
                System.out.println("\"" + sc.nextLine() + "\" �� �������� ������" + "\n");
                continue;
            }
            do {
                System.out.println("�������� �������� ��������:");
                System.out.println("1 - ��������\n2 - ���������\n3 - ���������\n4 - �������");
                choice = (int) sc.nextInt();
                switch (choice) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        try {
                            System.out.println("������� ������ �����:");
                            db2 = (double) sc.nextDouble();
                        } catch (InputMismatchException exc) {
                            System.out.println("\"" + sc.nextLine() + "\" �� �������� ������" + "\n");
                            continue;
                        }
                        if (choice == 1) {
                            System.out.println(db1 + " + " + db2 + " = " + (db1 + db2) + "\n");
                        } else if (choice == 2) {
                            System.out.println(db1 + " - " + db2 + " = " + (db1 - db2) + "\n");
                        } else if (choice == 3) {
                            System.out.println(db1 + " * " + db2 + " = " + (db1 * db2) + "\n");
                        } else if (choice == 4) {
                            if (Double.isInfinite(db1 / db2)) {
                                if (db2 == 0) {
                                    throw new ArithmeticException("Cannot divide by 0");
                                }
                            } else {
                                System.out.println(db1 + " / " + db2 + " = " + (db1 / db2) + "\n");
                            }
                        }
                }
            } while (choice < 1 | choice > 4);
        }
    }
}